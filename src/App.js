import React, { Component } from 'react';
import { BrowserRouter as Router, Route, Switch } from 'react-router-dom';
import { ChakraProvider } from "@chakra-ui/react";

import MainContext from "./utils/provider";
import Dataset from './models/dataset';
import SRCToScreens from './utils/screens';
import theme from "./utils/theme";

import UsersAPI from './models/api/request/users';

class App extends Component {
    constructor(props) {
        super(props)
        const temp = Dataset
        let auth = window.localStorage.getItem(temp.db)
        if (auth !== null) {
            auth = JSON.parse(auth)
            temp.is_auth = 1
            temp.auth = auth.api_key

            // Check Auth
            this.onCheckAuth(temp.db, auth)
        } else {
            const pathname = window.location.pathname
            if (pathname !== "/")
            window.location.replace("/")
        }

        this.state = temp
    }

    // OnCheckAuth
    onCheckAuth = (db, auth) => {
        UsersAPI.profile(auth.api_key, res => {
            if (res.code !== 200) {
                window.localStorage.removeItem(db)
                window.alert("Berhasil keluar dari aplikasi")
                window.location.replace("/")
                return
            }

            if (res.code === 200) {
                const pathname = window.location.pathname
                if (pathname === "/")
                window.location.replace("/dashboard")
                return
            }
        })
    }

    // Update State
    onUpdateState = (newState) => this.setState(newState)

    // Injector Display
    injectorDisplay = () => {
        const routes = []

        // Inject Side Routes
        if (this.state.is_auth !== 0) 
            this.state.menus.sidebar.forEach(item => {
                if (item.type === "auth")
                routes.push(item)
            })

        else this.state.menus.sidebar.forEach(item => {
            if (item.type !== "auth")
            routes.push(item)
        })

        return (
            <Switch>
                {routes.map((item) => {
                    return <Route exact path={item.src} component={SRCToScreens[item.aria]} 
                    key={"routing-" + item.id}/>
                })}
            </Switch>
        )
    }

    render = () => (
        <ChakraProvider theme={theme}>
            <MainContext.Provider value={{
                main_state: this.state,
                update_state: this.onUpdateState
            }}>
                <React.Fragment>
                    <Router>
                        {this.injectorDisplay()}
                    </Router>
                </React.Fragment>
            </MainContext.Provider>
        </ChakraProvider>
    )
}

export default App