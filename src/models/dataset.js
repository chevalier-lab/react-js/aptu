const Menus = {
    sidebar: [
        {
            id: "sidebar-menu-login",
            aria: "login",
            src: "/",
            type: "not-auth",
            label: "Login",
            user: ["*"]
        },
        {
            id: "sidebar-menu-dashboard",
            aria: "dashboard",
            src: "/dashboard",
            type: "auth",
            label: "Dashboard",
            user: ["*"]
        },
        {
            id: "sidebar-menu-position",
            aria: "position",
            src: "/position",
            type: "auth",
            label: "Kelola Jabatan",
            user: ["admin"]
        },
        {
            id: "sidebar-menu-users",
            aria: "users",
            src: "/users",
            type: "auth",
            label: "Kelola Pengguna",
            user: ["admin"]
        },
        {
            id: "sidebar-menu-employee",
            aria: "employee",
            src: "/employee",
            type: "auth",
            label: "Kelola Pegawai",
            user: ["admin"]
        },
        {
            id: "sidebar-menu-section",
            aria: "section",
            src: "/section",
            type: "auth",
            label: "Kelola Seksi",
            user: ["admin"]
        },
        {
            id: "sidebar-menu-approve-ticket",
            aria: "approveTicket",
            src: "/approveTicket",
            type: "auth",
            label: "Approval Tiket",
            user: ["apt"]
        },
        {
            id: "sidebar-menu-progress-ticket",
            aria: "progressTicket",
            src: "/progressTicket",
            type: "auth",
            label: "Proses Tiket",
            user: ["apt", "supervisor", "seksi"]
        },
        {
            id: "sidebar-menu-rating",
            aria: "rating",
            src: "/rating",
            type: "auth",
            label: "Hasil Rating",
            user: ["*"]
        },
    ],
}

const Images = {
    logo: "logo-kpknl.png",
    banner: "banner.jpg"
}

const Header = {
    logo: Images.logo,
    title: "PORTAL DASHBOARD",
    tagline: "Pelayanan Terpadu KPKNL Bandung",
}

const Dataset = {
    base_url: "http://213.190.4.40/storages/aptu/",
    menus: Menus,
    header: Header,
    images: Images,
    is_auth: 0,
    db: "kpknl-bandung",
    auth: "",
    range: (start, end) => {
        const temp = []
        for (let i = start; i <= end; i++) temp.push(i)
        return temp
    }
}

export default Dataset