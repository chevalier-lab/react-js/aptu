// Request Filter
const ReqFilter = {}
ReqFilter.toURI = (url = "", q={}) => {
    let newURL = url + "?" + Object.keys(q)
        .map(k => k + '=' + encodeURIComponent(q[k])).join('&')
    return newURL
}
ReqFilter.toJSON = (response) => response.json()

// Request Method
const ReqMethod = {
    GET: "GET",
    POST: "POST",
    PUT: "PUT",
    PATCH: "PATCH",
    DELETE: "DELETE",
}

// Request API Core
const ReqAPI = {}

// Request API Base URL
ReqAPI.base_url = "http://213.190.4.40/aptu-be/index.php/api"
ReqAPI.base_url_image = "http://213.190.4.40/aptu-be"
// ReqAPI.base_url = "http://localhost:2024/api"
// ReqAPI.base_url_image = "http://localhost:2024"

// Request API Auth
ReqAPI.auth = {}
ReqAPI.auth.base = ReqAPI.base_url + "/users"
ReqAPI.auth.login = ReqAPI.auth.base + "/login_pegawai"

// Request API User
ReqAPI.users = {}
ReqAPI.users.base = ReqAPI.base_url + "/users"
ReqAPI.users.create = ReqAPI.users.base + "/create"
ReqAPI.users.profile = ReqAPI.users.base + "/profile"
ReqAPI.users.update = (id=-1) => `${ReqAPI.users.base}/update/${id}` 
ReqAPI.users.delete = (id=-1) => `${ReqAPI.users.base}/delete/${id}` 
ReqAPI.users.filter = (page=1, q="") => 
    ReqFilter.toURI(ReqAPI.users.base, {
        page: page,
        search: q
    })

// Request API Jabatan
ReqAPI.jabatan = {}
ReqAPI.jabatan.base = ReqAPI.base_url + "/jabatan"
ReqAPI.jabatan.create = ReqAPI.jabatan.base + "/create"
ReqAPI.jabatan.update = (id=-1) => `${ReqAPI.jabatan.base}/update/${id}`
ReqAPI.jabatan.delete = (id=-1) => `${ReqAPI.jabatan.base}/delete/${id}`
ReqAPI.jabatan.filter = (page=1, q="") => 
    ReqFilter.toURI(ReqAPI.jabatan.base, {
        page: page,
        search: q
    })

// Request API Pegawai
ReqAPI.pegawai = {}
ReqAPI.pegawai.base = ReqAPI.base_url + "/pegawai"
ReqAPI.pegawai.create = ReqAPI.pegawai.base + "/create"
ReqAPI.pegawai.analytics = ReqAPI.pegawai.base + "/analytics"
ReqAPI.pegawai.update = (id=-1) => `${ReqAPI.pegawai.base}/update/${id}`
ReqAPI.pegawai.delete = (id=-1) => `${ReqAPI.pegawai.base}/delete/${id}`
ReqAPI.pegawai.filter = (page=1, q="") => 
    ReqFilter.toURI(ReqAPI.pegawai.base, {
        page: page,
        search: q
    })

// Request API Rating
ReqAPI.rating = {}
ReqAPI.rating.base = ReqAPI.base_url + "/rating"
ReqAPI.rating.create = (id=-1) => `${ReqAPI.rating.base}/create/${id}`
ReqAPI.rating.detail = (id=-1) => `${ReqAPI.rating.base}/detail/${id}`
ReqAPI.rating.delete = (id=-1) => `${ReqAPI.rating.base}/delete/${id}`
ReqAPI.rating.filter = (page=1, q="") => 
    ReqFilter.toURI(ReqAPI.rating.base, {
        page: page,
        search: q
    })

// Request API Seksi
ReqAPI.seksi = {}
ReqAPI.seksi.base = ReqAPI.base_url + "/seksi"
ReqAPI.seksi.create = ReqAPI.seksi.base + "/create"
ReqAPI.seksi.update = (id=-1) => `${ReqAPI.seksi.base}/update/${id}`
ReqAPI.seksi.delete = (id=-1) => `${ReqAPI.seksi.base}/delete/${id}`
ReqAPI.seksi.filter = (page=1, q="") => 
    ReqFilter.toURI(ReqAPI.seksi.base, {
        page: page,
        search: q
    })

// Request API Seksi Pegawai
ReqAPI.seksi.pegawai = {}
ReqAPI.seksi.pegawai.base = ReqAPI.seksi.base + "/pegawai"
ReqAPI.seksi.pegawai.create = (id=-1) => `${ReqAPI.seksi.pegawai.base}_create/${id}`
ReqAPI.seksi.pegawai.delete = (id=-1) => `${ReqAPI.seksi.pegawai.base}_delete/${id}`
ReqAPI.seksi.pegawai.filter = (id=-1, page=1, q="") => 
    ReqFilter.toURI(ReqAPI.seksi.pegawai.base + "/" + id, {
        page: page,
        search: q
    })

// Request API Tiket
ReqAPI.tiket = {}
ReqAPI.tiket.base = ReqAPI.base_url + "/tiket"
ReqAPI.tiket.task = ReqAPI.tiket.base + "/task"
ReqAPI.tiket.waiting = ReqAPI.tiket.base + "/waiting"
ReqAPI.tiket.update = (id=-1) => `${ReqAPI.tiket.base}/update/${id}`
ReqAPI.tiket.delete = (id=-1) => `${ReqAPI.tiket.base}/delete/${id}`
ReqAPI.tiket.approve = (id=-1) => `${ReqAPI.tiket.base}/approve/${id}`
ReqAPI.tiket.filter = (page=1, q="") => 
    ReqFilter.toURI(ReqAPI.tiket.base, {
        page: page,
        search: q
    })

export {
    ReqAPI,
    ReqMethod,
    ReqFilter
}