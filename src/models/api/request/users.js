import { ReqAPI, ReqMethod, ReqFilter } from "../req"

const UsersAPI = {}
UsersAPI.create = (data={}, callback) => fetch(
    ReqAPI.users.create, {
        method: ReqMethod.POST,
        headers: {
            "Content-Type": "application/json; charset=UTF-8"
        },
        body: JSON.stringify(data)
    }).then(ReqFilter.toJSON)
    .then(item => callback(item))
    .catch(error => callback(error))

UsersAPI.update = (id, data={}, callback) => fetch(
    ReqAPI.users.update(id), {
        method: ReqMethod.POST,
        headers: {
            "Content-Type": "application/json; charset=UTF-8"
        },
        body: JSON.stringify(data)
    }).then(ReqFilter.toJSON)
    .then(item => callback(item))
    .catch(error => callback(error))

UsersAPI.filter = (page=0, q="", callback) => fetch(
    ReqAPI.users.filter(page, q), {
        method: ReqMethod.GET
    }).then(ReqFilter.toJSON)
    .then(item => callback(item))
    .catch(error => callback(error))

UsersAPI.profile = (api_key="", callback) => fetch(
    ReqFilter.toURI(ReqAPI.users.profile, {
        api_key: api_key
    }), {
        method: ReqMethod.GET
    }).then(ReqFilter.toJSON)
    .then(item => callback(item))
    .catch(error => callback(error))

UsersAPI.delete = (id, callback) => fetch(
    ReqAPI.users.delete(id), {
        method: ReqMethod.GET,
    }).then(ReqFilter.toJSON)
    .then(item => callback(item))
    .catch(error => callback(error))

export default UsersAPI