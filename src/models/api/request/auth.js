import { ReqAPI, ReqMethod, ReqFilter } from "../req"

const AuthAPI = {}
AuthAPI.login = (uname="", passw="", callback) => fetch(
    ReqAPI.auth.login, {
        method: ReqMethod.POST,
        headers: {
            "Content-Type": "application/json; charset=UTF-8"
        },
        body: JSON.stringify({
            username: uname,
            password: passw
        })
    }).then(ReqFilter.toJSON)
    .then(item => callback(item))
    .catch(error => callback(error))

export default AuthAPI