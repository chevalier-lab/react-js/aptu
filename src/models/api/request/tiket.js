import { ReqAPI, ReqMethod, ReqFilter } from "../req"

const TiketAPI = {}
TiketAPI.task = (api_key="", callback) => fetch(
    ReqFilter.toURI(ReqAPI.tiket.task, {
        api_key: api_key
    }), {
        method: ReqMethod.GET
    }).then(ReqFilter.toJSON)
    .then(item => callback(item))
    .catch(error => callback(error))

TiketAPI.update = (api_key="", id, data={}, callback) => fetch(
    ReqFilter.toURI(ReqAPI.tiket.update(id), {
        api_key: api_key
    }), {
        method: ReqMethod.POST,
        headers: {
            "Content-Type": "application/json; charset=UTF-8"
        },
        body: JSON.stringify(data)
    }).then(ReqFilter.toJSON)
    .then(item => callback(item))
    .catch(error => callback(error))

TiketAPI.approve = (api_key="", id, callback) => fetch(
    ReqFilter.toURI(ReqAPI.tiket.approve(id), {
        "api_key": api_key
    }), {
        method: ReqMethod.GET
    }).then(ReqFilter.toJSON)
    .then(item => callback(item))
    .catch(error => callback(error))

TiketAPI.filter = (page=0, q="", callback) => fetch(
    ReqAPI.tiket.filter(page, q), {
        method: ReqMethod.GET
    }).then(ReqFilter.toJSON)
    .then(item => callback(item))
    .catch(error => callback(error))

TiketAPI.waiting = (page=0, q="", callback) => fetch(
    ReqFilter.toURI(ReqAPI.tiket.waiting, {
        page: page,
        search: q
    }), {
        method: ReqMethod.GET
    }).then(ReqFilter.toJSON)
    .then(item => callback(item))
    .catch(error => callback(error))

export default TiketAPI