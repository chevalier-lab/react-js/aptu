import { ReqAPI, ReqMethod, ReqFilter } from "../req"

const SeksiAPI = {}
SeksiAPI.create = (data={}, callback) => fetch(
    ReqAPI.seksi.create, {
        method: ReqMethod.POST,
        headers: {
            "Content-Type": "application/json; charset=UTF-8"
        },
        body: JSON.stringify(data)
    }).then(ReqFilter.toJSON)
    .then(item => callback(item))
    .catch(error => callback(error))

SeksiAPI.update = (id, data={}, callback) => fetch(
    ReqAPI.seksi.update(id), {
        method: ReqMethod.POST,
        headers: {
            "Content-Type": "application/json; charset=UTF-8"
        },
        body: JSON.stringify(data)
    }).then(ReqFilter.toJSON)
    .then(item => callback(item))
    .catch(error => callback(error))

SeksiAPI.filter = (page=0, q="", callback) => fetch(
    ReqAPI.seksi.filter(page, q), {
        method: ReqMethod.GET
    }).then(ReqFilter.toJSON)
    .then(item => callback(item))
    .catch(error => callback(error))

SeksiAPI.delete = (id, callback) => fetch(
    ReqAPI.seksi.delete(id), {
        method: ReqMethod.GET,
    }).then(ReqFilter.toJSON)
    .then(item => callback(item))
    .catch(error => callback(error))

export default SeksiAPI