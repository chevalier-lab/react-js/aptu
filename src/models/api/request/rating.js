import { ReqAPI, ReqMethod, ReqFilter } from "../req"

const RatingAPI = {}
RatingAPI.detail = (id, callback) => fetch(
    ReqAPI.rating.detail(id), {
        method: ReqMethod.GET
    }).then(ReqFilter.toJSON)
    .then(item => callback(item))
    .catch(error => callback(error))

RatingAPI.filter = (page=0, q="", callback) => fetch(
    ReqAPI.rating.filter(page, q), {
        method: ReqMethod.GET
    }).then(ReqFilter.toJSON)
    .then(item => callback(item))
    .catch(error => callback(error))

export default RatingAPI