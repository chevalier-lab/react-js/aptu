import { ReqAPI, ReqMethod, ReqFilter } from "../req"

const SeksiPegawaiAPI = {}
SeksiPegawaiAPI.create = (id, data={}, callback) => fetch(
    ReqAPI.seksi.pegawai.create(id), {
        method: ReqMethod.POST,
        headers: {
            "Content-Type": "application/json; charset=UTF-8"
        },
        body: JSON.stringify(data)
    }).then(ReqFilter.toJSON)
    .then(item => callback(item))
    .catch(error => callback(error))

SeksiPegawaiAPI.delete = (id, callback) => fetch(
    ReqAPI.seksi.pegawai.delete(id), {
        method: ReqMethod.GET
    }).then(ReqFilter.toJSON)
    .then(item => callback(item))
    .catch(error => callback(error))

SeksiPegawaiAPI.filter = (id, page=0, q="", callback) => fetch(
    ReqAPI.seksi.pegawai.filter(id, page, q), {
        method: ReqMethod.GET
    }).then(ReqFilter.toJSON)
    .then(item => callback(item))
    .catch(error => callback(error))

export default SeksiPegawaiAPI