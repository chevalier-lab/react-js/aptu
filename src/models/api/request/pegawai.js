import { ReqAPI, ReqMethod, ReqFilter } from "../req"

const PegawaiAPI = {}
PegawaiAPI.create = (data={}, callback) => fetch(
    ReqAPI.pegawai.create, {
        method: ReqMethod.POST,
        headers: {
            "Content-Type": "application/json; charset=UTF-8"
        },
        body: JSON.stringify(data)
    }).then(ReqFilter.toJSON)
    .then(item => callback(item))
    .catch(error => callback(error))

PegawaiAPI.update = (id, data={}, callback) => fetch(
    ReqAPI.pegawai.update(id), {
        method: ReqMethod.POST,
        headers: {
            "Content-Type": "application/json; charset=UTF-8"
        },
        body: JSON.stringify(data)
    }).then(ReqFilter.toJSON)
    .then(item => callback(item))
    .catch(error => callback(error))

PegawaiAPI.filter = (page=0, q="", callback) => fetch(
    ReqAPI.pegawai.filter(page, q), {
        method: ReqMethod.GET
    }).then(ReqFilter.toJSON)
    .then(item => callback(item))
    .catch(error => callback(error))

PegawaiAPI.filterUsingSeksi = (page=0, q="", callback) => fetch(
    ReqFilter.toURI(ReqAPI.pegawai.base + "/seksi", {
        page: page,
        search: q
    }), {
        method: ReqMethod.GET
    }).then(ReqFilter.toJSON)
    .then(item => callback(item))
    .catch(error => callback(error))

PegawaiAPI.filterUsingSupervisor = (page=0, q="", callback) => fetch(
    ReqFilter.toURI(ReqAPI.pegawai.base + "/supervisor", {
        page: page,
        search: q
    }), {
        method: ReqMethod.GET
    }).then(ReqFilter.toJSON)
    .then(item => callback(item))
    .catch(error => callback(error))

PegawaiAPI.analytics = (callback) => fetch(
    ReqAPI.pegawai.analytics, {
        method: ReqMethod.GET
    }).then(ReqFilter.toJSON)
    .then(item => callback(item))
    .catch(error => callback(error))

PegawaiAPI.delete = (id, callback) => fetch(
    ReqAPI.pegawai.delete(id), {
        method: ReqMethod.GET,
    }).then(ReqFilter.toJSON)
    .then(item => callback(item))
    .catch(error => callback(error))

export default PegawaiAPI