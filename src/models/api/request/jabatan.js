import { ReqAPI, ReqMethod, ReqFilter } from "../req"

const JabatanAPI = {}
JabatanAPI.create = (data={}, callback) => fetch(
    ReqAPI.jabatan.create, {
        method: ReqMethod.POST,
        headers: {
            "Content-Type": "application/json; charset=UTF-8"
        },
        body: JSON.stringify(data)
    }).then(ReqFilter.toJSON)
    .then(item => callback(item))
    .catch(error => callback(error))

JabatanAPI.update = (id, data={}, callback) => fetch(
    ReqAPI.jabatan.update(id), {
        method: ReqMethod.POST,
        headers: {
            "Content-Type": "application/json; charset=UTF-8"
        },
        body: JSON.stringify(data)
    }).then(ReqFilter.toJSON)
    .then(item => callback(item))
    .catch(error => callback(error))

JabatanAPI.delete = (id, callback) => fetch(
    ReqAPI.jabatan.delete(id), {
        method: ReqMethod.GET,
    }).then(ReqFilter.toJSON)
    .then(item => callback(item))
    .catch(error => callback(error))

JabatanAPI.filter = (page=0, q="", callback) => fetch(
    ReqAPI.jabatan.filter(page, q), {
        method: ReqMethod.GET
    }).then(ReqFilter.toJSON)
    .then(item => callback(item))
    .catch(error => callback(error))

export default JabatanAPI