import React from "react"
import {
    Box,
    Text,
    Button,
    Input,
    Modal,
    ModalOverlay,
    ModalContent,
    ModalHeader,
    ModalFooter,
    ModalBody,
    ModalCloseButton,
} from "@chakra-ui/react"

import { useToast } from "@chakra-ui/toast"

import Toast from "../../../components/toast"

import JabatanAPI from "../../../models/api/request/jabatan"

const CreateView = ({isOpen, onClose}) => {
    const [isDoCreate, setIsDoCreate] = React.useState(false)
    const labelRef = React.useRef()

    const toast = Toast(useToast())

    const doCreate = () => {
        if (labelRef.current.value === "") 
            return toast.error("Gagal Membuat", "Jabatan tidak boleh kosong")

        setIsDoCreate(true)
        JabatanAPI.create({
            label: labelRef.current.value
        }, res => {
            if (res.code === 200) {
                toast.success("Berhasil Membuat", res.message)
                onClose()
            } else toast.error("Gagal Membuat", res.message)
            
            setIsDoCreate(false)
        })
    }
    
    return (
        <Modal isOpen={isOpen} onClose={onClose}>
            <ModalOverlay />
            <ModalContent>
                <ModalHeader>+ Jabatan</ModalHeader>
                <ModalCloseButton />
                <ModalBody>
                    <Box>

                        <Text fontWeight="semibold">Nama Jabatan</Text>
                        <Input type="text" placeholder="Masukkan Nama Jabatan"
                        size="lg"
                        ref={labelRef} />
                
                    </Box>
                </ModalBody>

                <ModalFooter>
                    <Button colorScheme="blue" mr={3} onClick={doCreate}
                        isLoading={isDoCreate}>
                        Simpan
                    </Button>
                </ModalFooter>
            </ModalContent>
        </Modal>
    )
}

export default CreateView