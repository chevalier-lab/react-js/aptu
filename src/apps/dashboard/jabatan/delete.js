import React from "react"
import {
    Text,
    Button,
    Modal,
    ModalOverlay,
    ModalContent,
    ModalHeader,
    ModalFooter,
    ModalBody,
    ModalCloseButton,
} from "@chakra-ui/react"

import { useToast } from "@chakra-ui/toast"

import Toast from "../../../components/toast"

import JabatanAPI from "../../../models/api/request/jabatan"

const DeleteView = ({data, isOpen, onClose}) => {
    const [isDoDelete, setIsDoDelete] = React.useState(false)

    const toast = Toast(useToast())

    const doDelete = () => {
        setIsDoDelete(true)
        JabatanAPI.delete(data.id, res => {
            if (res.code === 200) {
                toast.success("Berhasil Menghapus", res.message)
                onClose()
            } else toast.error("Gagal Menghapus", res.message)
            
            setIsDoDelete(false)
        })
    }
    
    return (
        <Modal isOpen={isOpen} onClose={onClose}>
            <ModalOverlay />
            <ModalContent>
                <ModalHeader>Hapus Jabatan</ModalHeader>
                <ModalCloseButton />
                <ModalBody>
                    <center>

                        <Text>
                            Apa anda yakin ingin menghapus jabatan <strong>{(data !== null) ? data.label : ""}</strong>?<br />
                            Ini mungkin akan berdampak pada pegawai dan data lainnya yang berkaitan dengan jabatan tersebut.
                        </Text>
                
                    </center>
                </ModalBody>

                <ModalFooter>
                    <Button colorScheme="blue" mr={3} onClick={doDelete}
                        isLoading={isDoDelete}>
                        Hapus
                    </Button>
                </ModalFooter>
            </ModalContent>
        </Modal>
    )
}

export default DeleteView