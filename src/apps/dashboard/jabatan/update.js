import React from "react"
import {
    Box,
    Text,
    Button,
    Input,
    Modal,
    ModalOverlay,
    ModalContent,
    ModalHeader,
    ModalFooter,
    ModalBody,
    ModalCloseButton,
} from "@chakra-ui/react"

import { useToast } from "@chakra-ui/toast"

import Toast from "../../../components/toast"

import JabatanAPI from "../../../models/api/request/jabatan"

const UpdateView = ({data, isOpen, onClose}) => {
    const [isDoUpdate, setIsDoUpdate] = React.useState(false)
    const labelRef = React.useRef()

    const toast = Toast(useToast())

    const doUpdate = () => {
        if (labelRef.current.value === "") 
            return toast.error("Gagal Membuat", "Jabatan tidak boleh kosong")

        setIsDoUpdate(true)
        JabatanAPI.update(data.id, {
            label: labelRef.current.value
        }, res => {
            if (res.code === 200) {
                toast.success("Berhasil Membuat", res.message)
                onClose()
            } else toast.error("Gagal Membuat", res.message)
            
            setIsDoUpdate(false)
        })
    }
    
    return (
        <Modal isOpen={isOpen} onClose={onClose}>
            <ModalOverlay />
            <ModalContent>
                <ModalHeader>Ubah Jabatan</ModalHeader>
                <ModalCloseButton />
                <ModalBody>
                    <Box>

                        <Text fontWeight="semibold">Nama Jabatan</Text>
                        <Input type="text" placeholder="Masukkan Nama Jabatan"
                        size="lg"
                        defaultValue={(data !== null) ? data.label : ""}
                        ref={labelRef} />
                
                    </Box>
                </ModalBody>

                <ModalFooter>
                    <Button colorScheme="blue" mr={3} onClick={doUpdate}
                        isLoading={isDoUpdate}>
                        Simpan
                    </Button>
                </ModalFooter>
            </ModalContent>
        </Modal>
    )
}

export default UpdateView