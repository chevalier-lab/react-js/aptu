import React from "react"

import MainContext from "../../utils/provider"
import DashboardUseCase from "../../usecase/dashboard"

import ListView from "./progress_tiket/list"

const TiketProgressController = () => {

    return (
        <MainContext.Consumer>
            { provider => <React.Fragment>

                <DashboardUseCase render={_ => (
                    <>
                        <ListView provider={provider} />
                    </>
                )} />

                </React.Fragment> }
        </MainContext.Consumer>
    )
}

export default TiketProgressController