import React from "react"
import {
    Box,
    Text,
    Button,
    Input,
    Modal,
    ModalOverlay,
    ModalContent,
    ModalHeader,
    ModalFooter,
    ModalBody,
    ModalCloseButton,
} from "@chakra-ui/react"

import { useToast } from "@chakra-ui/toast"

import Toast from "../../../components/toast"

import TiketAPI from "../../../models/api/request/tiket"

const UpdateView = ({provider, data, isOpen, onClose}) => {
    const [isDoUpdate, setIsDoUpdate] = React.useState(false)

    const toast = Toast(useToast())

    const doUpdate = () => {

        let auth = window.localStorage.getItem(provider.main_state.db)
        if (auth !== null) {
            auth = JSON.parse(auth)
        }

        setIsDoUpdate(true)
        TiketAPI.approve(auth.api_key, data.id, res => {
            if (res.code === 200) {
                toast.success("Berhasil Mengubah", res.message)
                onClose()
            } else toast.error("Gagal Mengubah", res.message)
            
            setIsDoUpdate(false)
        })
    }
    
    return (
        <Modal isOpen={isOpen} onClose={onClose}>
            <ModalOverlay />
            <ModalContent>
                <ModalHeader>Approval Tiket?</ModalHeader>
                <ModalCloseButton />
                <ModalBody>
                    <Box>

                        <Text>Apakah anda yakin ingin menyetujui dan memproses tiket?</Text>
                
                    </Box>
                </ModalBody>

                <ModalFooter>
                    <Button colorScheme="blue" mr={3} onClick={doUpdate}
                        isLoading={isDoUpdate}>
                        Aprrove
                    </Button>
                </ModalFooter>
            </ModalContent>
        </Modal>
    )
}

export default UpdateView