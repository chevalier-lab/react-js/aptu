import React from "react"
import {
    Box,
    Table,
    Thead,
    Tbody,
    Tr,
    Td,
    Th,
    Text,
    Button,
    Flex,
    Input,
    ButtonGroup,
    IconButton,
    Center,
    Spacer
} from "@chakra-ui/react"

import {
    ChevronRightIcon,
    ChevronLeftIcon
} from "@chakra-ui/icons"

import TiketAPI from "../../../models/api/request/tiket"
import DetailView from "./detail"
import UpdateView from "./update"

const TableList = ({provider}) => {
    const [isLoadItem, setIsLoadItem] = React.useState(false)
    const [items, setItems] = React.useState([])
    const [meta, setMeta] = React.useState(null)
    const [selectedItem, setSelectedItem] = React.useState(null)

    const [isOpenUpdate, setIsOpenUpdate] = React.useState(false)
    const [isOpenDetail, setIsOpenDetail] = React.useState(false)
    const doRequestItems = React.useCallback(() => {
        loadItems(meta.page, meta.search)
    }, [isLoadItem])

    const loadItems = (page=0, q="") => {
        if (isLoadItem) return

        setIsLoadItem(true)
        TiketAPI.waiting(page, q, res => {
            if (res.code === 200) {

                setItems(res.data)
                setMeta(res.meta)

            } else setItems([])
            setIsLoadItem(false)
        })
    }

    React.useEffect(() => {
        loadItems(0, "")
    }, [])

    const setupBody = () => {
        if (isLoadItem) return (
            <Tbody>
                <Tr>
                    <Td colSpan={6}>Sedang memuat ...</Td>
                </Tr>
            </Tbody>
        )

        if (items.length === 0) return (
            <Tbody>
                <Tr>
                    <Td colSpan={6}>Belum ada data, tekan "+ Tiket" untuk menambahkan.</Td>
                </Tr>
            </Tbody>
        )

        return (
            <Tbody>
                {items.map((item) => {
                    return (
                        <Tr key={"item-list-tiket-" + item.id}>
                            <Td>{item.no}</Td>
                            <Td>{item.created_at}</Td>
                            <Td>{item.pemohon}</Td>
                            <Td>{item.kepentingan}</Td>
                            <Td>{item.status.toString().toUpperCase()}</Td>
                            <Td>
                                <Button size="xs" colorScheme="blue"
                                    isLoading={isOpenDetail}
                                    marginX={1}
                                    onClick={() => {
                                        setSelectedItem(item)
                                        setIsOpenDetail(true)
                                    }}>Detail</Button>

                                <Button size="xs" colorScheme="yellow"
                                    isLoading={isOpenUpdate}
                                    marginX={1}
                                    onClick={() => {
                                        setSelectedItem(item)
                                        setIsOpenUpdate(true)
                                    }}>Approve</Button>
                            </Td>
                        </Tr>
                    )
                })}
            </Tbody>
        )
    }

    const setupPagination = () => {
        if (isLoadItem || items.length === 0) return (<div></div>)
        
        let pagination = []
        const totalPage = meta.totalPage + 1
        const page = Number(meta.page) + 1

        if (totalPage < 6) pagination = [...provider.main_state.range(1, totalPage)]
        else if (page + 2 <= totalPage) pagination = [...provider.main_state.range((page - 2), (page + 2))]
        else pagination = [...provider.main_state.range((totalPage - 5), totalPage)]

        return <Flex paddingX={4} paddingY={2}>
            <Center>
                <Text fontSize="sm">Halaman: {page} dari {totalPage}</Text>
            </Center>
            <Spacer />
            <Box>
                <ButtonGroup size="sm" isAttached variant="outline"
                    borderRadius="0">
                    <IconButton icon={<ChevronLeftIcon />} />
                    {pagination.map((item, position) => <Button mr="-px" bgColor={(item === page) ? "green.200" : "white"}
                    key={"pagination-device-log-" + position}
                    onClick={() => {
                        let temp = meta
                        temp.page = `${page - 1}`
                        setMeta(temp)

                        doRequestItems()
                    }}>{item}</Button>)}
                    <IconButton icon={<ChevronRightIcon />} />
                </ButtonGroup>
            </Box>
        </Flex>
    }
    
    return (
        <Box
            bgColor="white"
            margin={8}>

            <Flex borderBottom="1px solid rgba(0,0,0,0.1)">
                <Input variant="unstyled" placeholder="Cari tiket ..."
                size="md"
                flex={1}
                paddingX={4}
                paddingY={2} />
            </Flex>

            <DetailView data={selectedItem} isOpen={isOpenDetail} onClose={() => {
                setIsOpenDetail(false)
            }} />

            <UpdateView data={selectedItem} isOpen={isOpenUpdate} onClose={() => {
                setIsOpenUpdate(false)
                doRequestItems()
            }} provider={provider} />

            <Table variant="striped">

                <Thead>
                    <Tr>
                        <Th>No</Th>
                        <Th>Tanggal</Th>
                        <Th>Pemohon</Th>
                        <Th>Kepentingan</Th>
                        <Th>Status</Th>
                        <Th>Aksi</Th>
                    </Tr>
                </Thead>

                { setupBody() }

            </Table>

            { setupPagination() }

        </Box>
    )
}

const ListView = ({provider}) => {
    
    const setupDisplay = () => {

        return (
            <Box maxW="100%"
                display="block"
                overflowX="auto"
                overflowY="scroll"
                height="100%">

                <Text fontSize="2xl"
                    marginTop={8}
                    marginX={8}
                    fontWeight="semibold"
                    textColor="green.700">Approval Tiket</Text>

                <TableList provider={provider} />

            </Box>
        )
    }

    return ( <React.Fragment>
        { setupDisplay() }
    </React.Fragment> )
}

export default ListView