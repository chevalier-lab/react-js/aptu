import React from "react"
import {
    Box,
    Text,
    Modal,
    ModalOverlay,
    ModalContent,
    ModalHeader,
    ModalBody,
    ModalCloseButton,
    Flex
} from "@chakra-ui/react"

const DetailView = ({data, isOpen, onClose}) => {
    
    return (
        <Modal isOpen={isOpen} onClose={onClose}
        size={"2xl"}>
            <ModalOverlay />
            <ModalContent>
                <ModalHeader>Detail Tiket</ModalHeader>
                <ModalCloseButton />
                <ModalBody>
                    <Box>

                        <Box marginBottom={2}>
                            <Text fontWeight="semibold">No Tiket</Text>
                            <Text
                            fontSize={"20px"}>{(data !== null) ? data.no : "-"}</Text>
                        </Box>
                        
                        <Flex marginBottom={2}>
                            <Box flex={1}>
                                <Text fontWeight="semibold">Tanggal Buat</Text>
                                <Text>{(data !== null) ? data.created_at : "-"}</Text>
                            </Box>
                            <Box flex={1}>
                                <Text fontWeight="semibold">Tanggal Ubah</Text>
                                <Text>{(data !== null) ? data.updated_at : "-"}</Text>
                            </Box>
                        </Flex>
                        
                        <Box marginBottom={2}>
                            <Text fontWeight="semibold">Pemohon</Text>
                            <Text>{(data !== null) ? data.pemohon : "-"}</Text>
                        </Box>
                        
                        <Flex marginBottom={2}>
                            <Box flex={1}>
                                <Text fontWeight="semibold">Email</Text>
                                <Text>{(data !== null) ? data.email : "-"}</Text>
                            </Box>

                            <Box flex={1}>
                                <Text fontWeight="semibold">Nomor Telepon</Text>
                                <Text>{(data !== null) ? data.phone : "-"}</Text>
                            </Box>
                        </Flex>

                        <Box marginBottom={2}>
                            <Text fontWeight="semibold">Alamat</Text>
                            <Text>{(data !== null) ? data.alamat : "-"}</Text>
                        </Box>

                        <Box marginBottom={2}>
                            <Text fontWeight="semibold">Kepentingan</Text>
                            <Text>{(data !== null) ? data.kepentingan : "-"}</Text>
                        </Box>
                        
                        <Box marginBottom={2}>
                            <Text fontWeight="semibold">Ringkasan</Text>
                            <Text>{(data !== null) ? data.ringkasan : "-"}</Text>
                        </Box>

                        <Box marginBottom={2}>
                            <Text fontWeight="semibold">Status</Text>
                            <Text>{(data !== null) ? data.status : "-"}</Text>
                        </Box>

                    </Box>
                </ModalBody>
            </ModalContent>
        </Modal>
    )
}

export default DetailView