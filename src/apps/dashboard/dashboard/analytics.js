import React from "react"

import {
    Grid,
    GridItem,
    Box,
    Text
} from "@chakra-ui/react"

import PegawaiAPI from "../../../models/api/request/pegawai"

const Analytics = ({provider}) => {

    const [data, setData] = React.useState({
        pegawai: 0,
        client: 0,
        tiket: {
            antrian: 0,
            proses: 0,
            selesai: 0
        },
        rating: {
            total: 0,
            avg: 0
        }
    })

    React.useEffect(() => {
        PegawaiAPI.analytics(res => {
            if (res.code === 200) {
                setData(res.data)
            }
        })
    }, [])

    return (
        <React.Fragment>

            <Box>
                <Text fontSize="2xl"
                    marginTop={8}
                    marginX={8}
                    fontWeight="semibold"
                    textColor="green.700">Dashboard</Text>

                <Grid templateColumns="repeat(3, 1fr)" gap={6}
                    marginX={8}
                    marginY={4}
                    fontWeight="semibold"
                    fontSize="xl">
                    <GridItem>
                        <Box
                            bgColor="white"
                            border="1px solid rgba(0,0,0,0.1)"
                            paddingX={2}
                            paddingY={4}
                            textAlign="center">
                            {data.pegawai} <br />Pegawai
                        </Box>
                    </GridItem>
                    
                    <GridItem>
                        <Box
                            bgColor="white"
                            border="1px solid rgba(0,0,0,0.1)"
                            paddingX={2}
                            paddingY={4}
                            textAlign="center">
                            {data.client} <br />Klien
                        </Box>
                    </GridItem>
                    
                    <GridItem>
                        <Box
                            bgColor="white"
                            border="1px solid rgba(0,0,0,0.1)"
                            paddingX={2}
                            paddingY={4}
                            textAlign="center">
                            {data.rating.total} <br />Pengguna Memberi Rating
                        </Box>
                    </GridItem>
                    
                    <GridItem>
                        <Box
                            bgColor="white"
                            border="1px solid rgba(0,0,0,0.1)"
                            paddingX={2}
                            paddingY={4}
                            textAlign="center">
                            {data.rating.avg} <br />Rata - Rata Rating
                        </Box>
                    </GridItem>
                    
                    <GridItem>
                        <Box
                            bgColor="white"
                            border="1px solid rgba(0,0,0,0.1)"
                            paddingX={2}
                            paddingY={4}
                            textAlign="center">
                            {data.tiket.antrian} <br />Tiket Antrian
                        </Box>
                    </GridItem>
                    
                    <GridItem>
                        <Box
                            bgColor="white"
                            border="1px solid rgba(0,0,0,0.1)"
                            paddingX={2}
                            paddingY={4}
                            textAlign="center">
                            {data.tiket.proses} <br />Tiket Proses
                        </Box>
                    </GridItem>
                    
                    <GridItem>
                        <Box
                            bgColor="white"
                            border="1px solid rgba(0,0,0,0.1)"
                            paddingX={2}
                            paddingY={4}
                            textAlign="center">
                            {data.tiket.selesai} <br />Tiket Selesai
                        </Box>
                    </GridItem>
                </Grid>
            </Box>

        </React.Fragment>
    )
}

export default Analytics