import React from "react"
import {
    Box,
    Text,
    Button,
    List,
    ListItem,
    Input,
    Modal,
    ModalOverlay,
    ModalContent,
    ModalHeader,
    ModalFooter,
    ModalBody,
    ModalCloseButton,
} from "@chakra-ui/react"

import { useToast } from "@chakra-ui/toast"

import Toast from "../../../components/toast"

import PegawaiAPI from "../../../models/api/request/pegawai"
import TiketAPI from "../../../models/api/request/tiket"

const AlihkanView = ({provider, data, isOpen, onClose}) => {
    const [isDoUpdate, setIsDoUpdate] = React.useState(false)

    const [pegawai, setPegawai] = React.useState([])
    const [selectedPegawai, setSelectedPegawai] = React.useState(null)
    const [searchPegawai, setSearchPegawai] = React.useState("")
    
    const auth = JSON.parse(window.localStorage.getItem(provider.main_state.db))

    const [isDoLoadPegawaiItems, setIsDoLoadPegawaiItem] = React.useState(false)
    const doRequestPegawaiItems = React.useCallback(() => {

        if (isDoLoadPegawaiItems) return

        setIsDoLoadPegawaiItem(true)
        if (auth.level === "apt") {
            PegawaiAPI.filterUsingSupervisor(0, searchPegawai, res => {
                if (res.code === 200) {
                    setPegawai(res.data)
                }
    
                setIsDoLoadPegawaiItem(false)
            })
        } else {
            PegawaiAPI.filterUsingSeksi(0, searchPegawai, res => {
                if (res.code === 200) {
                    setPegawai(res.data)
                }
    
                setIsDoLoadPegawaiItem(false)
            })
        }

    }, [isDoLoadPegawaiItems])

    const toast = Toast(useToast())

    const doUpdate = () => {
        if (selectedPegawai === null) 
            return toast.error("Gagal Mengirim", "Pegawai tidak boleh kosong")
        
        setIsDoUpdate(true)
        TiketAPI.update(auth.api_key, data.id, {
            id_solver: selectedPegawai.id_users,
            status: (auth.level === "apt") ? "diproses supervisor" : "diproses seksi"
        }, res => {
            if (res.code === 200) {
                toast.success("Berhasil Mengirim", res.message)
                onClose()
            } else toast.error("Gagal Mengirim", res.message)
            
            setIsDoUpdate(false)
        })
    }

    const setupItemPegawai = (isSelected=false, item, position) => {
        return (
            <ListItem key={"item-pegawai-selected-" + position}
                bgColor={isSelected ? "green.200" : "white"}
                _hover={{
                    bgColor: "green.200"
                }}
                padding={2}
                border="1px solid rgba(0,0,0,0.1)"
                marginY={1}
                onClick={() => {
                    setSelectedPegawai(item)
                    setSearchPegawai(item.nama + "("+ item.label +")")
                }}>
                {item.nama + "("+ item.label +")"}
            </ListItem>
        )
    }

    const setupListPegawai = () => {

        if (selectedPegawai !== null) {
            return (
                <List>
                    {pegawai.map((item, position) => {
                        if (selectedPegawai.id === item.id)
                        return setupItemPegawai(true, item, position)

                        else
                        return setupItemPegawai(false, item, position)
                    })}
                </List>
            )
        }

        return (
            <List>
                {pegawai.map((item, position) => {
                    return setupItemPegawai(false, item, position)
                })}
            </List>
        )
    }
    
    return (
        <Modal isOpen={isOpen} onClose={onClose}>
            <ModalOverlay />
            <ModalContent>
                <ModalHeader>Alihkan</ModalHeader>
                <ModalCloseButton />
                <ModalBody>
                    <Box>

                        <Text fontWeight="semibold">Pilih {auth.level === "apt" ? "Supervisor" : "Seksi"}</Text>
                        <Input type="text" placeholder="Pilih akan dialihkan ke-"
                        size="lg"
                        value={searchPegawai}
                        onChange={(e) => {
                            setSearchPegawai(e.target.value)
                            doRequestPegawaiItems()
                        }}
                        onClick={doRequestPegawaiItems} />

                        { setupListPegawai() }
                
                    </Box>
                </ModalBody>

                <ModalFooter>
                    <Button colorScheme="blue" mr={3} onClick={doUpdate}
                        isLoading={isDoUpdate}>
                        Kirim
                    </Button>
                </ModalFooter>
            </ModalContent>
        </Modal>
    )
}

export default AlihkanView