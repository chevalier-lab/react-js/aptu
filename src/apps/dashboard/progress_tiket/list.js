import React from "react"
import {
    Box,
    Table,
    Thead,
    Tbody,
    Tr,
    Td,
    Th,
    Text,
    Button,
    Flex,
    Input,
} from "@chakra-ui/react"

import TiketAPI from "../../../models/api/request/tiket"

import ReplyView from "./reply"
import AlihkanView from "./alihkan"

const TableList = ({provider}) => {
    const [isLoadItem, setIsLoadItem] = React.useState(false)
    const [items, setItems] = React.useState([])
    const [selectedItem, setSelectedItem] = React.useState(null)
    
    const auth = JSON.parse(window.localStorage.getItem(provider.main_state.db))

    const [isOpenUpdate, setIsOpenUpdate] = React.useState(false)
    const [isOpenReply, setIsOpenReply] = React.useState(false)
    const doRequestItems = React.useCallback(() => {
        loadItems()
    }, [isLoadItem])

    const loadItems = () => {
        if (isLoadItem) return

        let auth = window.localStorage.getItem(provider.main_state.db)
        if (auth !== null) {
            auth = JSON.parse(auth)
        }

        setIsLoadItem(true)
        TiketAPI.task(auth.api_key, res => {
            if (res.code === 200) {

                setItems(res.data)

            } else setItems([])
            setIsLoadItem(false)
        })
    }

    React.useEffect(() => {
        loadItems()
    }, [])

    const setupBody = () => {
        if (isLoadItem) return (
            <Tbody>
                <Tr>
                    <Td colSpan={6}>Sedang memuat ...</Td>
                </Tr>
            </Tbody>
        )

        if (items.length === 0) return (
            <Tbody>
                <Tr>
                    <Td colSpan={6}>Belum ada data, tekan "+ Tiket" untuk menambahkan.</Td>
                </Tr>
            </Tbody>
        )

        return (
            <Tbody>
                {items.map((item) => {
                    return (
                        <Tr key={"item-list-tiket-" + item.id}>
                            <Td>{item.no}</Td>
                            <Td>{item.created_at}</Td>
                            <Td>{item.pemohon}</Td>
                            <Td>{item.kepentingan}</Td>
                            <Td>{item.status.toString().toUpperCase()}</Td>
                            <Td>
                                {auth.level !== "seksi" ? <Button size="xs" colorScheme="green"
                                isLoading={isOpenUpdate}
                                marginX={1}
                                onClick={() => {
                                    setSelectedItem(item)
                                    setIsOpenUpdate(true)
                                }}>Alihkan</Button> : <span></span>}

                                <Button size="xs" colorScheme="yellow"
                                    isLoading={isOpenReply}
                                    marginX={1}
                                    onClick={() => {
                                        setSelectedItem(item)
                                        setIsOpenReply(true)
                                    }}>Balas</Button>
                            </Td>
                        </Tr>
                    )
                })}
            </Tbody>
        )
    }
    
    return (
        <Box
            bgColor="white"
            margin={8}>

            <Flex borderBottom="1px solid rgba(0,0,0,0.1)">
                <Input variant="unstyled" placeholder="Cari tiket ..."
                size="md"
                flex={1}
                paddingX={4}
                paddingY={2} />
            </Flex>

            <ReplyView provider={provider} data={selectedItem} isOpen={isOpenReply}
                onClose={() => {
                    setIsOpenReply(false);
                    doRequestItems()
                }} />

            <AlihkanView provider={provider} data={selectedItem} isOpen={isOpenUpdate}
                onClose={() => {
                    setIsOpenUpdate(false)
                    doRequestItems()
                }} />

            <Table variant="striped">

                <Thead>
                    <Tr>
                        <Th>No</Th>
                        <Th>Tanggal</Th>
                        <Th>Pemohon</Th>
                        <Th>Kepentingan</Th>
                        <Th>Status</Th>
                        <Th>Aksi</Th>
                    </Tr>
                </Thead>

                { setupBody() }

            </Table>

        </Box>
    )
}

const ListView = ({provider}) => {
    
    const setupDisplay = () => {

        return (
            <Box maxW="100%"
                display="block"
                overflowX="auto"
                overflowY="scroll"
                height="100%">

                <Text fontSize="2xl"
                    marginTop={8}
                    marginX={8}
                    fontWeight="semibold"
                    textColor="green.700">Proses Tiket</Text>

                <TableList provider={provider} />

            </Box>
        )
    }

    return ( <React.Fragment>
        { setupDisplay() }
    </React.Fragment> )
}

export default ListView