import React from "react"
import {
    Box,
    Text,
    Button,
    Textarea,
    Modal,
    ModalOverlay,
    ModalContent,
    ModalHeader,
    ModalFooter,
    ModalBody,
    ModalCloseButton,
} from "@chakra-ui/react"

import { useToast } from "@chakra-ui/toast"

import Toast from "../../../components/toast"

import TiketAPI from "../../../models/api/request/tiket"

const ReplyView = ({provider, data, isOpen, onClose}) => {
    const [isDoUpdate, setIsDoUpdate] = React.useState(false)
    const labelRef = React.useRef()

    const toast = Toast(useToast())

    const doUpdate = () => {
        if (labelRef.current.value === "") 
            return toast.error("Gagal Mengirim", "Balasan tidak boleh kosong")

        let auth = window.localStorage.getItem(provider.main_state.db)
        if (auth !== null) {
            auth = JSON.parse(auth)
        }
        
        setIsDoUpdate(true)
        TiketAPI.update(auth.api_key, data.id, {
            balasan: labelRef.current.value,
            status: "selesai"
        }, res => {
            if (res.code === 200) {
                toast.success("Berhasil Mengirim", res.message)
                onClose()
            } else toast.error("Gagal Mengirim", res.message)
            
            setIsDoUpdate(false)
        })
    }
    
    return (
        <Modal isOpen={isOpen} onClose={onClose}>
            <ModalOverlay />
            <ModalContent>
                <ModalHeader>Kirim Balasan</ModalHeader>
                <ModalCloseButton />
                <ModalBody>
                    <Box>

                        <Text fontWeight="semibold">Balasan</Text>
                        <Textarea placeholder="Masukkan Balasan"
                        size="lg"
                        ref={labelRef}>{(data !== null) ? data.balasan : ""}</Textarea>
                
                    </Box>
                </ModalBody>

                <ModalFooter>
                    <Button colorScheme="blue" mr={3} onClick={doUpdate}
                        isLoading={isDoUpdate}>
                        Kirim
                    </Button>
                </ModalFooter>
            </ModalContent>
        </Modal>
    )
}

export default ReplyView