import React from "react"

import MainContext from "../../utils/provider"
import DashboardUseCase from "../../usecase/dashboard"

import ListView from "./users/list"

const UsersController = () => {

    return (
        <MainContext.Consumer>
            { provider => <React.Fragment>

                <DashboardUseCase render={_ => (
                    <>
                        <ListView provider={provider} />
                    </>
                )} />

                </React.Fragment> }
        </MainContext.Consumer>
    )
}

export default UsersController