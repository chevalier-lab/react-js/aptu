import React from "react"
import {
    Text,
    Button,
    Modal,
    ModalOverlay,
    ModalContent,
    ModalHeader,
    ModalFooter,
    ModalBody,
    ModalCloseButton,
} from "@chakra-ui/react"

import { useToast } from "@chakra-ui/toast"

import Toast from "../../../components/toast"

import PegawaiAPI from "../../../models/api/request/pegawai"

const DeleteView = ({data, isOpen, onClose}) => {
    const [isDoDelete, setIsDoDelete] = React.useState(false)

    const toast = Toast(useToast())

    const doDelete = () => {
        setIsDoDelete(true)
        PegawaiAPI.delete(data.id, res => {
            if (res.code === 200) {
                toast.success("Berhasil Menghapus", res.message)
                onClose()
            } else toast.error("Gagal Menghapus", res.message)
            
            setIsDoDelete(false)
        })
    }
    
    return (
        <Modal isOpen={isOpen} onClose={onClose}>
            <ModalOverlay />
            <ModalContent>
                <ModalHeader>Hapus Pegawai</ModalHeader>
                <ModalCloseButton />
                <ModalBody>
                    <center>

                        <Text>
                            Apa anda yakin ingin menghapus pegawai <strong>{(data !== null) ? data.nama : ""}</strong>?<br />
                            Ini mungkin akan berdampak pada data lainnya yang berkaitan dengan pegawai tersebut.
                        </Text>
                
                    </center>
                </ModalBody>

                <ModalFooter>
                    <Button colorScheme="blue" mr={3} onClick={doDelete}
                        isLoading={isDoDelete}>
                        Hapus
                    </Button>
                </ModalFooter>
            </ModalContent>
        </Modal>
    )
}

export default DeleteView