import React from "react"
import {
    Box,
    Text,
    Button,
    Input,
    Modal,
    ModalOverlay,
    ModalContent,
    ModalHeader,
    ModalFooter,
    ModalBody,
    ModalCloseButton,
    Select,
    List,
    ListItem
} from "@chakra-ui/react"

import { useToast } from "@chakra-ui/toast"

import Toast from "../../../components/toast"

import PegawaiAPI from "../../../models/api/request/pegawai"
import UsersAPI from "../../../models/api/request/users"
import JabatanAPI from "../../../models/api/request/jabatan"

const CreateView = ({isOpen, onClose}) => {
    const [isDoCreate, setIsDoCreate] = React.useState(false)
    const nipRef = React.useRef()
    const fullNameRef = React.useRef()
    const jkRef = React.useRef()

    const [users, setUsers] = React.useState([])
    const [selectedUser, setSelectedUser] = React.useState(null)
    const [searchUser, setSearchUser] = React.useState("")

    const [jabatan, setJabatan] = React.useState([])
    const [selectedJabatan, setSelectedJabatan] = React.useState(null)
    const [searchJabatan, setSearchJabatan] = React.useState("")

    const [isDoLoadUserItems, setIsDoLoadUserItem] = React.useState(false)
    const doRequestUserItems = React.useCallback(() => {

        if (isDoLoadUserItems) return

        setIsDoLoadUserItem(true)
        UsersAPI.filter(0, searchUser, res => {
            if (res.code === 200) {
                setUsers(res.data)
            }

            setIsDoLoadUserItem(false)
        })

    }, [isDoLoadUserItems])

    const [isDoLoadJabatanItems, setIsDoLoadJabatanItems] = React.useState(false)
    const doRequestJabatanItems = React.useCallback(() => {

        if (isDoLoadJabatanItems) return

        setIsDoLoadJabatanItems(true)
        JabatanAPI.filter(0, searchJabatan, res => {
            if (res.code === 200) {
                setJabatan(res.data)
            }

            setIsDoLoadJabatanItems(false)
        })

    }, [isDoLoadJabatanItems])

    const toast = Toast(useToast())

    const doCreate = () => {
        if (nipRef.current.value === "") 
            return toast.error("Gagal Membuat", "NIP tidak boleh kosong")
        else if (fullNameRef.current.value === "") 
            return toast.error("Gagal Membuat", "Nama lengkap tidak boleh kosong")
        else if (jkRef.current.value === "") 
            return toast.error("Gagal Membuat", "Jenis kelamin tidak boleh kosong")
        else if (selectedUser === null) 
            return toast.error("Gagal Membuat", "Pengguna tidak boleh kosong")
        else if (selectedJabatan === null) 
            return toast.error("Gagal Membuat", "Jabatan tidak boleh kosong")

        setIsDoCreate(true)
        PegawaiAPI.create({
            nama: fullNameRef.current.value,
            nip: nipRef.current.value,
            jk: jkRef.current.value,
            id_users: selectedUser.id,
            id_jabatan: selectedJabatan.id,
        }, res => {
            if (res.code === 200) {
                toast.success("Berhasil Membuat", res.message)
                onClose()
            } else toast.error("Gagal Membuat", res.message)
            
            setIsDoCreate(false)
        })
    }

    const setupItemUser = (isSelected=false, item, position) => {
        return (
            <ListItem key={"item-users-selected-" + position}
                bgColor={isSelected ? "green.200" : "white"}
                _hover={{
                    bgColor: "green.200"
                }}
                padding={2}
                border="1px solid rgba(0,0,0,0.1)"
                marginY={1}
                onClick={() => {
                    setSelectedUser(item)
                    setSearchUser(item.full_name + "("+ item.username +")")
                }}>
                {item.full_name + "("+ item.username +")"}
            </ListItem>
        )
    }

    const setupListUsers = () => {

        if (selectedUser !== null) {
            return (
                <List>
                    {users.map((item, position) => {
                        if (selectedUser.id === item.id)
                        return setupItemUser(true, item, position)

                        else
                        return setupItemUser(false, item, position)
                    })}
                </List>
            )
        }

        return (
            <List>
                {users.map((item, position) => {
                    return setupItemUser(false, item, position)
                })}
            </List>
        )
    }

    const setupItemJabatan = (isSelected=false, item, position) => {
        return (
            <ListItem key={"item-jabatan-selected-" + position}
                bgColor={isSelected ? "green.200" : "white"}
                _hover={{
                    bgColor: "green.200"
                }}
                padding={2}
                border="1px solid rgba(0,0,0,0.1)"
                marginY={1}
                onClick={() => {
                    setSelectedJabatan(item)
                    setSearchJabatan(item.label)
                }}>
                {item.label}
            </ListItem>
        )
    }

    const setupListJabatan = () => {
        if (selectedJabatan !== null) {
            return (
                <List>
                    {jabatan.map((item, position) => {
                        if (selectedJabatan.id === item.id)
                        return setupItemJabatan(true, item, position)

                        else
                        return setupItemJabatan(false, item, position)
                    })}
                </List>
            )
        }

        return (
            <List>
                {jabatan.map((item, position) => {
                    return setupItemJabatan(false, item, position)
                })}
            </List>
        )
    }
    
    return (
        <Modal isOpen={isOpen} onClose={onClose}>
            <ModalOverlay />
            <ModalContent>
                <ModalHeader>+ Pegawai</ModalHeader>
                <ModalCloseButton />
                <ModalBody>
                    <Box>

                        <Text fontWeight="semibold">NIP</Text>
                        <Input type="number" placeholder="Masukkan NIP"
                        size="lg"
                        ref={nipRef} />

                        <br />
                        <br />

                        <Text fontWeight="semibold">Nama Lengkap</Text>
                        <Input type="text" placeholder="Masukkan Nama Lengkap"
                        size="lg"
                        ref={fullNameRef} />

                        <br />
                        <br />
                        
                        <Text fontWeight="semibold">Jenis Kelamin</Text>
                        <Select placeholder="Pilih jenis Kelamin"
                        size="lg"
                        ref={jkRef}>
                            <option value="m">Pria</option>
                            <option value="f">Wanita</option>
                        </Select>

                        <br />
                        
                        <Text fontWeight="semibold">Pengguna</Text>
                        <Input type="text" placeholder="Pilih Akun Pengguna"
                        size="lg"
                        value={searchUser}
                        onChange={(e) => {
                            setSearchUser(e.target.value)
                            doRequestUserItems()
                        }}
                        onClick={doRequestUserItems} />

                        { setupListUsers() }

                        <br />
                        
                        <Text fontWeight="semibold">Jabatan</Text>
                        <Input type="text" placeholder="Pilih Jabatan Pegawai"
                        size="lg"
                        value={searchJabatan}
                        onChange={(e) => {
                            setSearchJabatan(e.target.value)
                            doRequestJabatanItems()
                        }}
                        onClick={doRequestJabatanItems} />

                        { setupListJabatan() }
                
                    </Box>
                </ModalBody>

                <ModalFooter>
                    <Button colorScheme="blue" mr={3} onClick={doCreate}
                        isLoading={isDoCreate}>
                        Simpan
                    </Button>
                </ModalFooter>
            </ModalContent>
        </Modal>
    )
}

export default CreateView