import React from "react"
import {
    Box,
    Text,
    Button,
    Input,
    Modal,
    ModalOverlay,
    ModalContent,
    ModalHeader,
    ModalFooter,
    ModalBody,
    ModalCloseButton,
    Select
} from "@chakra-ui/react"

import { useToast } from "@chakra-ui/toast"

import Toast from "../../../components/toast"

import UsersAPI from "../../../models/api/request/users"

const UpdateView = ({data, isOpen, onClose}) => {
    const [isDoUpdate, setIsDoUpdate] = React.useState(false)
    const fullNameRef = React.useRef()
    const levelRef = React.useRef()

    const toast = Toast(useToast())

    const doUpdate = () => {
        if (fullNameRef.current.value === "") 
            return toast.error("Gagal Membuat", "Nama lengkap tidak boleh kosong")
        else if (levelRef.current.value === "") 
            return toast.error("Gagal Membuat", "Level tidak boleh kosong")

        setIsDoUpdate(true)
        UsersAPI.update(data.id, {
            full_name: fullNameRef.current.value,
            level: levelRef.current.value,
        }, res => {
            if (res.code === 200) {
                toast.success("Berhasil Membuat", res.message)
                onClose()
            } else toast.error("Gagal Membuat", res.message)
            
            setIsDoUpdate(false)
        })
    }
    
    return (
        <Modal isOpen={isOpen} onClose={onClose}>
            <ModalOverlay />
            <ModalContent>
                <ModalHeader>Ubah Pengguna</ModalHeader>
                <ModalCloseButton />
                <ModalBody>
                    <Box>

                        <Text fontWeight="semibold">Nama Lengkap</Text>
                        <Input type="text" placeholder="Masukkan Nama Lengkap"
                        size="lg"
                        defaultValue={(data !== null) ? data.full_name : ""}
                        ref={fullNameRef} />

                        <br />
                        <br />
                        
                        <Text fontWeight="semibold">Level Pengguna</Text>
                        <Select placeholder="Pilih Level Pengguna"
                        size="lg"
                        defaultValue={(data !== null) ? data.level : ""}
                        ref={levelRef}>
                            <option value="apt">APT</option>
                            <option value="supervisor">Supervisor</option>
                            <option value="seksi">Seksi</option>
                            <option value="management">Manajemen</option>
                            <option value="admin">Administrator</option>
                        </Select>
                
                    </Box>
                </ModalBody>

                <ModalFooter>
                    <Button colorScheme="blue" mr={3} onClick={doUpdate}
                        isLoading={isDoUpdate}>
                        Simpan
                    </Button>
                </ModalFooter>
            </ModalContent>
        </Modal>
    )
}

export default UpdateView