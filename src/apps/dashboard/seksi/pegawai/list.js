import React from "react"
import {
    Box,
    Table,
    Thead,
    Tbody,
    Tr,
    Td,
    Th,
    Text,
    Button,
    Flex,
    Input,
    ButtonGroup,
    IconButton,
    Center,
    Spacer
} from "@chakra-ui/react"

import {
    ChevronRightIcon,
    ChevronLeftIcon
} from "@chakra-ui/icons"

import SeksiPegawaiAPI from "../../../../models/api/request/seksi_pegawai"
import CreateView from "./create"
import DeleteView from "./delete"

const TableList = ({data, provider}) => {
    const [isLoadItem, setIsLoadItem] = React.useState(false)
    const [items, setItems] = React.useState([])
    const [meta, setMeta] = React.useState(null)
    const [selectedItem, setSelectedItem] = React.useState(null)
    const [search, setSearch] = React.useState("")

    const [isOpenAdd, setIsOpenAdd] = React.useState(false)
    const [isOpenDelete, setIsOpenDelete] = React.useState(false)
    const doRequestItems = React.useCallback(() => {
        if (meta !== null)
        loadItems(meta.page, search)
    }, [isLoadItem])

    const loadItems = (page=0, q="") => {
        if (isLoadItem) return

        setIsLoadItem(true)
        SeksiPegawaiAPI.filter(data.id, page, q, res => {
            if (res.code === 200) {

                setItems(res.data)
                setMeta(res.meta)

            } else setItems([])
            setIsLoadItem(false)
        })
    }

    React.useEffect(() => {
        loadItems(0, "")
    }, [])

    const setupBody = () => {
        if (isLoadItem) return (
            <Tbody>
                <Tr>
                    <Td colSpan={6}>Sedang memuat ...</Td>
                </Tr>
            </Tbody>
        )

        if (items.length === 0) return (
            <Tbody>
                <Tr>
                    <Td colSpan={6}>Belum ada data, tekan "+ Pegawai" untuk menambahkan.</Td>
                </Tr>
            </Tbody>
        )

        return (
            <Tbody>
                {items.map((item, position) => {
                    return (
                        <Tr key={"item-list-pegawai-" + item.id}>
                            <Td>{(position + 1)}</Td>
                            <Td>{item.nip}</Td>
                            <Td>{item.nama}</Td>
                            <Td>{item.jk === "m" ? "Pria" : "Wanita"}</Td>
                            <Td>{item.label}</Td>
                            <Td>
                                <Button size="xs" colorScheme="red"
                                    isLoading={isOpenDelete}
                                    onClick={() => {
                                        setSelectedItem(item)
                                        setIsOpenDelete(true)
                                    }}>Hapus</Button>
                            </Td>
                        </Tr>
                    )
                })}
            </Tbody>
        )
    }

    const setupPagination = () => {
        if (isLoadItem || items.length === 0) return (<div></div>)
        
        let pagination = []
        const totalPage = meta.totalPage + 1
        const page = Number(meta.page) + 1

        if (totalPage < 6) pagination = [...provider.main_state.range(1, totalPage)]
        else if (page + 2 <= totalPage) pagination = [...provider.main_state.range((page - 2), (page + 2))]
        else pagination = [...provider.main_state.range((totalPage - 5), totalPage)]

        return <Flex paddingX={4} paddingY={2}>
            <Center>
                <Text fontSize="sm">Halaman: {page} dari {totalPage}</Text>
            </Center>
            <Spacer />
            <Box>
                <ButtonGroup size="sm" isAttached variant="outline"
                    borderRadius="0">
                    <IconButton icon={<ChevronLeftIcon />} />
                    {pagination.map((item, position) => <Button mr="-px" bgColor={(item === page) ? "green.200" : "white"}
                    key={"pagination-device-log-" + position}
                    onClick={() => {
                        let temp = meta
                        temp.page = `${page - 1}`
                        setMeta(temp)

                        doRequestItems()
                    }}>{item}</Button>)}
                    <IconButton icon={<ChevronRightIcon />} />
                </ButtonGroup>
            </Box>
        </Flex>
    }
    
    return (
        <Box
            bgColor="white"
            margin={8}>

            <Flex borderBottom="1px solid rgba(0,0,0,0.1)">
                <Button colorScheme="green" size="md"
                borderRadius="0"
                isLoading={isOpenAdd}
                onClick={() => setIsOpenAdd(true)}>
                    + Pegawai
                </Button>

                <Input variant="unstyled" placeholder="Cari pegawai ..."
                size="md"
                flex={1}
                value={search}
                onChange={(e) => {
                    setSearch(e.target.value)
                    doRequestItems()
                }}
                paddingX={4} />
            </Flex>

            <CreateView data={data} isOpen={isOpenAdd} onClose={() => {
                setIsOpenAdd(false)
                doRequestItems()
            }} />

            <DeleteView data={selectedItem} isOpen={isOpenDelete} onClose={() => {
                setIsOpenDelete(false)
                doRequestItems()
            }} />

            <Table variant="striped">

                <Thead>
                    <Tr>
                        <Th>No</Th>
                        <Th>NIP</Th>
                        <Th>Nama Lengkap</Th>
                        <Th>Jenis Kelamin</Th>
                        <Th>Jabatan</Th>
                        <Th>Aksi</Th>
                    </Tr>
                </Thead>

                { setupBody() }

            </Table>

            { setupPagination() }

        </Box>
    )
}

const ListView = ({data, provider}) => {
    
    const setupDisplay = () => {

        return (
            <Box maxW="100%"
                display="block"
                overflowX="auto"
                overflowY="scroll"
                height="100%">

                <TableList provider={provider}
                    data={data} />

            </Box>
        )
    }

    return ( <React.Fragment>
        { setupDisplay() }
    </React.Fragment> )
}

export default ListView