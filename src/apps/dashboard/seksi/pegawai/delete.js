import React from "react"
import {
    Box,
    Text,
    Button,
} from "@chakra-ui/react"

import { useToast } from "@chakra-ui/toast"

import Toast from "../../../../components/toast"

import SeksiPegawaiAPI from "../../../../models/api/request/seksi_pegawai"

const DeleteView = ({data, isOpen, onClose}) => {
    const [isDoDelete, setIsDoDelete] = React.useState(false)

    const toast = Toast(useToast())

    const doDelete = () => {
        setIsDoDelete(true)
        SeksiPegawaiAPI.delete(data.id_parent, res => {
            if (res.code === 200) {
                toast.success("Berhasil Menghapus", res.message)
                onClose()
            } else toast.error("Gagal Menghapus", res.message)
            
            setIsDoDelete(false)
        })
    }
    
    return (
        <Box display={!isOpen ? "none" : "block"}
            marginY={4}>

            <Text fontWeight="semibold">Hapus Pegawai</Text>
            <Text>Apakah anda yaking ingin menghapus <strong>{(data !== null) ? data.nama : ""}</strong>?</Text>

            <br />

            <Button colorScheme="red" mr={3} onClick={doDelete}
                isLoading={isDoDelete}>
                Hapus
            </Button>

            <Button colorScheme="gray" mr={3} onClick={onClose}>
                Tutup
            </Button>
    
        </Box>
    )
}

export default DeleteView