import React from "react"
import {
    Box,
    Text,
    Button,
    Input,
    List,
    ListItem
} from "@chakra-ui/react"

import { useToast } from "@chakra-ui/toast"

import Toast from "../../../../components/toast"

import PegawaiAPI from "../../../../models/api/request/pegawai"
import SeksiPegawaiAPI from "../../../../models/api/request/seksi_pegawai"

const CreateView = ({data, isOpen, onClose}) => {
    const [isDoCreate, setIsDoCreate] = React.useState(false)

    const [pegawai, setPegawai] = React.useState([])
    const [selectedPegawai, setSelectedPegawai] = React.useState(null)
    const [searchPegawai, setSearchPegawai] = React.useState("")

    const [isDoLoadPegawaiItems, setIsDoLoadPegawaiItem] = React.useState(false)
    const doRequestPegawaiItems = React.useCallback(() => {

        if (isDoLoadPegawaiItems) return

        setIsDoLoadPegawaiItem(true)
        PegawaiAPI.filterUsingSeksi(0, searchPegawai, res => {
            if (res.code === 200) {
                setPegawai(res.data)
            }

            setIsDoLoadPegawaiItem(false)
        })

    }, [isDoLoadPegawaiItems])

    const toast = Toast(useToast())

    const doCreate = () => {
        if (selectedPegawai === null) 
            return toast.error("Gagal Membuat", "Pegawai tidak boleh kosong")

        setIsDoCreate(true)
        SeksiPegawaiAPI.create(data.id, {
            id_pegawai: selectedPegawai.id,
        }, res => {
            if (res.code === 200) {
                toast.success("Berhasil Membuat", res.message)
                onClose()
            } else toast.error("Gagal Membuat", res.message)
            
            setIsDoCreate(false)
        })
    }

    const setupItemPegawai = (isSelected=false, item, position) => {
        return (
            <ListItem key={"item-pegawai-selected-" + position}
                bgColor={isSelected ? "green.200" : "white"}
                _hover={{
                    bgColor: "green.200"
                }}
                padding={2}
                border="1px solid rgba(0,0,0,0.1)"
                marginY={1}
                onClick={() => {
                    setSelectedPegawai(item)
                    setSearchPegawai(item.nama + "("+ item.nip +")")
                }}>
                {item.nama + "("+ item.nip +")"}
            </ListItem>
        )
    }

    const setupListPegawai = () => {

        if (selectedPegawai !== null) {
            return (
                <List>
                    {pegawai.map((item, position) => {
                        if (selectedPegawai.id === item.id)
                        return setupItemPegawai(true, item, position)

                        else
                        return setupItemPegawai(false, item, position)
                    })}
                </List>
            )
        }

        return (
            <List>
                {pegawai.map((item, position) => {
                    return setupItemPegawai(false, item, position)
                })}
            </List>
        )
    }
    
    return (
        <Box display={!isOpen ? "none" : "block"}
            marginY={4}>

            <Text fontWeight="semibold">Pegawai</Text>
            <Input type="text" placeholder="Pilih Pegawai"
            size="lg"
            value={searchPegawai}
            onChange={(e) => {
                setSearchPegawai(e.target.value)
                doRequestPegawaiItems()
            }}
            onClick={doRequestPegawaiItems} />

            { setupListPegawai() }

            <br />

            <Button colorScheme="blue" mr={3} onClick={doCreate}
                isLoading={isDoCreate}>
                Simpan
            </Button>

            <Button colorScheme="red" mr={3} onClick={onClose}>
                Tutup
            </Button>
    
        </Box>
    )
}

export default CreateView