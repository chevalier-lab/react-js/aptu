import React from "react"
import {
    Box,
    Modal,
    ModalOverlay,
    ModalContent,
    ModalHeader,
    ModalBody,
    ModalCloseButton,
} from "@chakra-ui/react"

import ListView from "./list"

const InitView = ({data, provider, isOpen, onClose}) => {

    const setupContent = () => {
        if (data === null) return (<div></div>)

        return <ListView provider={provider} data={data} />
    }
    
    return (
        <Modal isOpen={isOpen} onClose={onClose} size="6xl">
            <ModalOverlay />
            <ModalContent>
                <ModalHeader>Kelola Pegawai Seksi</ModalHeader>
                <ModalCloseButton />
                <ModalBody>
                    { setupContent() }
                </ModalBody>
            </ModalContent>
        </Modal>
    )
}

export default InitView