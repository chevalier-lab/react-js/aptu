import React from "react"
import {
    Box,
    Table,
    Thead,
    Tbody,
    Tr,
    Td,
    Th,
    Text,
    Button,
    Flex,
    Input,
    ButtonGroup,
    IconButton,
    Center,
    Spacer
} from "@chakra-ui/react"

import {
    ChevronRightIcon,
    ChevronLeftIcon
} from "@chakra-ui/icons"

import SeksiAPI from "../../../models/api/request/seksi"
import CreateView from "./create"
import UpdateView from "./update"
import InitView from "./pegawai/init"
import DeleteView from "./delete"

const TableList = ({provider}) => {
    const [isLoadItem, setIsLoadItem] = React.useState(false)
    const [items, setItems] = React.useState([])
    const [meta, setMeta] = React.useState(null)
    const [selectedItem, setSelectedItem] = React.useState(null)

    const [isOpenAdd, setIsOpenAdd] = React.useState(false)
    const [isOpenUpdate, setIsOpenUpdate] = React.useState(false)
    const [isOpenInit, setIsOpenInit] = React.useState(false)
    const [isOpenDelete, setIsOpenDelete] = React.useState(false)
    const doRequestItems = React.useCallback(() => {
        loadItems(meta.page, meta.search)
    }, [isLoadItem])

    const loadItems = (page=0, q="") => {
        if (isLoadItem) return

        setIsLoadItem(true)
        SeksiAPI.filter(page, q, res => {
            if (res.code === 200) {

                setItems(res.data)
                setMeta(res.meta)

            } else setItems([])
            setIsLoadItem(false)
        })
    }

    React.useEffect(() => {
        loadItems(0, "")
    }, [])

    const setupBody = () => {
        if (isLoadItem) return (
            <Tbody>
                <Tr>
                    <Td colSpan={3}>Sedang memuat ...</Td>
                </Tr>
            </Tbody>
        )

        if (items.length === 0) return (
            <Tbody>
                <Tr>
                    <Td colSpan={3}>Belum ada data, tekan "+ Seksi" untuk menambahkan.</Td>
                </Tr>
            </Tbody>
        )

        return (
            <Tbody>
                {items.map((item, position) => {
                    return (
                        <Tr key={"item-list-seksi-" + item.id}>
                            <Td>{(position + 1)}</Td>
                            <Td>{item.label}</Td>
                            <Td>
                                <Button size="xs" colorScheme="yellow"
                                    isLoading={isOpenUpdate}
                                    marginX={1}
                                    onClick={() => {
                                        setSelectedItem(item)
                                        setIsOpenUpdate(true)
                                    }}>Ubah</Button>

                                <Button size="xs" colorScheme="red"
                                    isLoading={isOpenDelete}
                                    marginX={1}
                                    onClick={() => {
                                        setSelectedItem(item)
                                        setIsOpenDelete(true)
                                    }}>Hapus</Button>

                                <Button size="xs" colorScheme="blue"
                                    marginX={1}
                                    isLoading={isOpenInit}
                                    onClick={() => {
                                        setSelectedItem(item)
                                        setIsOpenInit(true)
                                    }}>Kelola Pegawai</Button>
                            </Td>
                        </Tr>
                    )
                })}
            </Tbody>
        )
    }

    const setupPagination = () => {
        if (isLoadItem || items.length === 0) return (<div></div>)
        
        let pagination = []
        const totalPage = meta.totalPage + 1
        const page = Number(meta.page) + 1

        if (totalPage < 6) pagination = [...provider.main_state.range(1, totalPage)]
        else if (page + 2 <= totalPage) pagination = [...provider.main_state.range((page - 2), (page + 2))]
        else pagination = [...provider.main_state.range((totalPage - 5), totalPage)]

        return <Flex paddingX={4} paddingY={2}>
            <Center>
                <Text fontSize="sm">Halaman: {page} dari {totalPage}</Text>
            </Center>
            <Spacer />
            <Box>
                <ButtonGroup size="sm" isAttached variant="outline"
                    borderRadius="0">
                    <IconButton icon={<ChevronLeftIcon />} />
                    {pagination.map((item, position) => <Button mr="-px" bgColor={(item === page) ? "green.200" : "white"}
                    key={"pagination-device-log-" + position}
                    onClick={() => {
                        let temp = meta
                        temp.page = `${page - 1}`
                        setMeta(temp)

                        doRequestItems()
                    }}>{item}</Button>)}
                    <IconButton icon={<ChevronRightIcon />} />
                </ButtonGroup>
            </Box>
        </Flex>
    }
    
    return (
        <Box
            bgColor="white"
            margin={8}>

            <CreateView isOpen={isOpenAdd} onClose={() => {
                setIsOpenAdd(false)
                doRequestItems()
            }} />

            <UpdateView isOpen={isOpenUpdate} onClose={() => {
                setIsOpenUpdate(false)
                doRequestItems()
            }} data={selectedItem} />

            <DeleteView isOpen={isOpenDelete} onClose={() => {
                setIsOpenDelete(false)
                doRequestItems()
            }} data={selectedItem} />

            <InitView provider={provider}
            isOpen={isOpenInit} onClose={() => {
                setIsOpenInit(false)
                doRequestItems()
            }} data={selectedItem} />

            <Flex borderBottom="1px solid rgba(0,0,0,0.1)">
                <Button colorScheme="green" size="md"
                borderRadius="0"
                onClick={() => setIsOpenAdd(true)}>
                    + Seksi
                </Button>

                <Input variant="unstyled" placeholder="Cari seksi ..."
                size="md"
                flex={1}
                paddingX={4} />
            </Flex>

            <Table variant="striped">

                <Thead>
                    <Tr>
                        <Th>No</Th>
                        <Th>Seksi</Th>
                        <Th>Aksi</Th>
                    </Tr>
                </Thead>

                { setupBody() }

            </Table>

            { setupPagination() }

        </Box>
    )
}

const ListView = ({provider}) => {
    
    const setupDisplay = () => {

        return (
            <Box maxW="100%"
                display="block"
                overflowX="auto"
                overflowY="scroll"
                height="100%">

                <Text fontSize="2xl"
                    marginTop={8}
                    marginX={8}
                    fontWeight="semibold"
                    textColor="green.700">Kelola Seksi</Text>

                <TableList provider={provider} />

            </Box>
        )
    }

    return ( <React.Fragment>
        { setupDisplay() }
    </React.Fragment> )
}

export default ListView