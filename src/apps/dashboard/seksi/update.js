import React from "react"
import {
    Box,
    Text,
    Button,
    Input,
    Modal,
    ModalOverlay,
    ModalContent,
    ModalHeader,
    ModalFooter,
    ModalBody,
    ModalCloseButton,
} from "@chakra-ui/react"

import { useToast } from "@chakra-ui/toast"

import Toast from "../../../components/toast"

import SeksiAPI from "../../../models/api/request/seksi"

const UpdateView = ({data, isOpen, onClose}) => {
    const [isDoUpdate, setIsDoUpdate] = React.useState(false)
    const labelRef = React.useRef()

    const toast = Toast(useToast())

    const doUpdate = () => {
        if (labelRef.current.value === "") 
            return toast.error("Gagal Mengubah", "Nama Seksi tidak boleh kosong")

        setIsDoUpdate(true)
        SeksiAPI.update(data.id, {
            label: labelRef.current.value
        }, res => {
            if (res.code === 200) {
                toast.success("Berhasil Mengubah", res.message)
                onClose()
            } else toast.error("Gagal Mengubah", res.message)
            
            setIsDoUpdate(false)
        })
    }
    
    return (
        <Modal isOpen={isOpen} onClose={onClose}>
            <ModalOverlay />
            <ModalContent>
                <ModalHeader>Ubah Seksi</ModalHeader>
                <ModalCloseButton />
                <ModalBody>
                    <Box>

                        <Text fontWeight="semibold">Nama Seksi</Text>
                        <Input type="text" placeholder="Masukkan Nama Seksi"
                        size="lg"
                        defaultValue={(data !== null) ? data.label : ""}
                        ref={labelRef} />
                
                    </Box>
                </ModalBody>

                <ModalFooter>
                    <Button colorScheme="blue" mr={3} onClick={doUpdate}
                        isLoading={isDoUpdate}>
                        Simpan
                    </Button>
                </ModalFooter>
            </ModalContent>
        </Modal>
    )
}

export default UpdateView