import React from "react"
import {
    Box,
    Image,
    Input,
    Text,
    Button
} from "@chakra-ui/react"
import { useToast } from "@chakra-ui/toast"

import Toast from "../../../components/toast"
import AuthAPI from "../../../models/api/request/auth"

const Login = ({provider}) => {
    const images = provider.main_state.images
    const storagetBaseURL = provider.main_state.base_url

    const usernameRef = React.useRef()
    const passwordRef = React.useRef()
    
    const toast = Toast(useToast())
    const [isDoLogin, setIsDoLogin] = React.useState(false)

    const doLogin = () => {
        if (usernameRef.current.value === "") 
            return toast.error("Gagal Masuk", "Username tidak boleh kosong")
        else if (passwordRef.current.value === "")
            return toast.error("Gagal Masuk", "Password tidak boleh kosong")

        setIsDoLogin(true)
        AuthAPI.login(usernameRef.current.value, passwordRef.current.value, res => {
            if (res.code === 200) {
                console.log(res.data)
                window.localStorage.setItem(provider.main_state.db, JSON.stringify(res.data))
                window.location.replace("/dashboard")
            } else toast.error("Gagal Masuk", res.message)

            setIsDoLogin(false)
        })
    }

    // =================================================

    const setupDisplay = () => {

        return (
            <Box
                bgColor="white"
                color="gray.700"
                position="absolute"
                top="50%"
                left="50%"
                transform="translate(-50%, -50%)"
                zIndex="2"
                width="container.sm"
                padding={8}
                borderRadius="8px">

                <center>
                    <Image src={storagetBaseURL + images.logo}
                    alt={images.logo}
                    maxW="360px" />
                </center>

                <Text fontWeight="semibold">Username/NIP</Text>
                <Input type="text" placeholder="Masukkan Username/NIP Anda"
                size="lg"
                ref={usernameRef} />
                
                <br />
                <br />

                <Text fontWeight="semibold">Password</Text>
                <Input type="password" placeholder="Masukkan Password Anda"
                size="lg"
                ref={passwordRef} />

                <br />
                <br />

                <Button isFullWidth={true} colorScheme="green"
                    size="lg"
                    isLoading={isDoLogin}
                    onClick={doLogin}>
                    Masuk
                </Button>

                <br />
                <br />

                <Text textColor="red.300">Belum memiliki akun? Hub. Admin</Text>

            </Box>
        )
    }

    const setupBackground = () => {

        return <Box height="100vh"
        width="100%"
        display="block"
        backgroundImage={`url('${storagetBaseURL + images.banner}')`}
        backgroundSize="cover"
        backgroundPosition="center"
        backgroundRepeat="no-repeat"
        filter="blur(8px)" />
    }

    return (<React.Fragment>
        <>
            { setupBackground() }
            { setupDisplay() }
        </>
    </React.Fragment>)
}

export default Login