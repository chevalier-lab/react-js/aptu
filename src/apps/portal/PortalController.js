import React from "react"

import MainContext from "../../utils/provider"
import PortalUseCase from "../../usecase/portal"
import Login from "./views/login"

const PortalController = () => {

    return (
        <MainContext.Consumer>
            { provider => <React.Fragment>

                <PortalUseCase render={_ => (
                    <>
                        <Login provider={provider} />            
                    </>
                )} />

                </React.Fragment> }
        </MainContext.Consumer>
    )
}

export default PortalController