import React from "react"
import { 
    Box, Flex
} from "@chakra-ui/react"

import MainContext from "../utils/provider"
import Sidebar from "../components/sidebar"

class DashboardUseCase extends React.Component {

    setupUseCase = (provider) => {
        return (
            <Flex
                bgColor="gray.50"
                height="100vh"
                width="100%"
                position="relative"
                overflowX="hidden">

                <Box
                    width="360px"
                    bgColor="#233819"
                    color="white"
                    borderRight="1px solid rgba(0,0,0,0.1)">
                    <Sidebar provider={provider} />
                </Box>

                <Box flex={1}>
                    {this.props.render(this.state)}
                </Box>

            </Flex>
        )
    }
    
    render = () => {
        return (
            <MainContext.Consumer>
                { provider => <React.Fragment>
                    { this.setupUseCase(provider) }
                </React.Fragment> }
            </MainContext.Consumer>
        )
    }
}

export default DashboardUseCase