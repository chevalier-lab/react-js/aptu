import PortalController from "../apps/portal/PortalController"
import DashboardController from "../apps/dashboard/DashboardController"
import JabatanController from "../apps/dashboard/JabatanController"
import UsersController from "../apps/dashboard/UsersController"
import PegawaiController from "../apps/dashboard/PegawaiController"
import SeksiController from "../apps/dashboard/SeksiController"
import TiketApproveController from "../apps/dashboard/TiketApproveController"
import TiketProgressController from "../apps/dashboard/TiketProgressController"
import RatingController from "../apps/dashboard/RatingController"

const SRCToScreens = {
    // Main
    login: PortalController,
    dashboard: DashboardController,
    position: JabatanController,
    users: UsersController,
    employee: PegawaiController,
    section: SeksiController,
    approveTicket: TiketApproveController,
    progressTicket: TiketProgressController,
    rating: RatingController,
}

export default SRCToScreens