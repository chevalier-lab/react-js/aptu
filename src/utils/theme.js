import { extendTheme } from "@chakra-ui/react"

const theme = extendTheme({
  config: {
    useSystemColorMode: true,
    initialColorMode: "light"
  }
});

export default theme