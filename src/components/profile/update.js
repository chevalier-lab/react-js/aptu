import React from "react"
import {
    Box,
    Text,
    Button,
    Input,
    Modal,
    ModalOverlay,
    ModalContent,
    ModalHeader,
    ModalFooter,
    ModalBody,
    ModalCloseButton,
} from "@chakra-ui/react"

import { useToast } from "@chakra-ui/toast"

import Toast from "../toast"

import UsersAPI from "../../models/api/request/users"

const UpdateView = ({provider, data, isOpen, onClose}) => {
    const [isDoUpdate, setIsDoUpdate] = React.useState(false)
    const fullNameRef = React.useRef()
    const usernameRef = React.useRef()
    const passwordRef = React.useRef()

    const toast = Toast(useToast())

    const doUpdate = () => {
        if (fullNameRef.current.value === "") 
            return toast.error("Gagal Menyimpan", "Nama lengkap tidak boleh kosong")
        else if (usernameRef.current.value === "") 
            return toast.error("Gagal Menyimpan", "Username tidak boleh kosong")

        const updateData = {
            full_name: fullNameRef.current.value,
            username: usernameRef.current.value,
        }

        if (passwordRef.current.value !== "")
            updateData.password = passwordRef.current.value

        setIsDoUpdate(true)
        UsersAPI.update(data.id, updateData, res => {
            if (res.code === 200) {
                toast.success("Berhasil Menyimpan", res.message)

                window.localStorage.setItem(provider.main_state.db, JSON.stringify(res.data))

                onClose(true)
            } else toast.error("Gagal Menyimpan", res.message)
            
            setIsDoUpdate(false)
        })
    }
    
    return (
        <Modal isOpen={isOpen} onClose={onClose}>
            <ModalOverlay />
            <ModalContent>
                <ModalHeader>Ubah Profil</ModalHeader>
                <ModalCloseButton />
                <ModalBody>
                    <Box>

                        <Text fontWeight="semibold">Nama Lengkap</Text>
                        <Input type="text" placeholder="Masukkan Nama Lengkap"
                        size="lg"
                        defaultValue={(data !== null) ? data.full_name : ""}
                        ref={fullNameRef} />

                        <Text fontWeight="semibold">Username</Text>
                        <Input type="text" placeholder="Masukkan Username"
                        size="lg"
                        defaultValue={(data !== null) ? data.username : ""}
                        ref={usernameRef} />

                        <Text fontWeight="semibold">Password</Text>
                        <Input type="text" placeholder="Masukkan Password Baru"
                        size="lg"
                        ref={passwordRef} />
                        <Text fontWeight="semibold"
                        textColor="red">Kosongkan jika password tetap</Text>
                
                    </Box>
                </ModalBody>

                <ModalFooter>
                    <Button colorScheme="blue" mr={3} onClick={doUpdate}
                        isLoading={isDoUpdate}>
                        Simpan
                    </Button>
                </ModalFooter>
            </ModalContent>
        </Modal>
    )
}

export default UpdateView