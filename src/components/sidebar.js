import React from "react"
import {
    Box,
    Image,
    List,
    ListItem,
    Button
} from "@chakra-ui/react"
import { Link } from 'react-router-dom';

import UpdateView from "./profile/update";

const SidebarItem = ({path, item}) => {
    let isActive = false
    if (item.src === path) isActive = true
    else if (!isActive && path !== "/") {
        if (item.src.includes(path)) isActive = true
        else if (item.src.includes(path.split("/")[1])) isActive = true
    }

    return (!isActive) ?
    <Link
        to={item.src}>
        <Button size="lg"
            isFullWidth={true}
            variant="ghost"
            textColor="white"
            flexDirection="row"
            justifyContent="flex-start"
            _hover={{
                bgColor: "#B3A8AC",
                textColor: "black"
            }}>
            {item.label}
        </Button>
    </Link> : 
    <Link
        to={item.src}>
        <Button size="lg"
            isFullWidth={true}
            variant="solid"
            bgColor="white"
            flexDirection="row"
            justifyContent="flex-start"
            textColor="green.400">
            {item.label}
        </Button>
    </Link>
}

const SidebarList = ({provider}) => {
    const main_state = provider.main_state
    const menus = main_state.menus.sidebar
    const [isOpenProfile, setIsOpenProfile] = React.useState(false)
    
    let auth = window.localStorage.getItem(main_state.db)
    if (auth !== null) {
        auth = JSON.parse(auth)
    } 
    
    const pathname = window.location.pathname
    const sidebar = []
    menus.forEach(item => {
        if (item.type === "auth") {
            if (item.user.includes("*") || item.user.includes(auth.level))
            sidebar.push(item)
        }
    })

    return (
        <List spacing={2}>
            { sidebar.map(item => {
                return <ListItem
                key={item.id}>
                    <SidebarItem item={item}
                    path={pathname} />
                </ListItem>
            }) }

            <ListItem>
                <Button size="lg"
                    isFullWidth={true}
                    variant="ghost"
                    textColor="white"
                    flexDirection="row"
                    justifyContent="flex-start"
                    _hover={{
                        bgColor: "#B3A8AC",
                        textColor: "black"
                    }}
                    onClick={() => {
                        setIsOpenProfile(true)
                    }}>
                    Profil
                </Button>

                <UpdateView provider={provider} data={auth}
                    isOpen={isOpenProfile} onClose={(isChange=false) => {
                        if (isChange) {
                            window.setTimeout(() => 
                            window.location.replace("/"), 1500);
                        }
                        setIsOpenProfile(false)
                    }} />
            </ListItem>

            <ListItem>
                <Button size="lg"
                    isFullWidth={true}
                    variant="solid"
                    colorScheme="red"
                    flexDirection="row"
                    justifyContent="flex-start"
                    onClick={() => {
                        window.localStorage.removeItem(provider.main_state.db)
                        window.location.replace("/")
                    }}>
                    Keluar
                </Button>
            </ListItem>
        </List>
    )
}

const Sidebar = ({provider}) => {

    const setupDisplay = () => {
        const main_state = provider.main_state
        const base_url = main_state.base_url
        const header = main_state.header

        return (
            <Box paddingY={8}
                paddingX={4}
                overflowY="auto"
                overflowX="hidden">

                <center>
                    <Image src={base_url + header.logo} maxW="200px" />
                </center>

                <br />
                <SidebarList provider={provider} />

            </Box>
        )
    }

    return (
        <React.Fragment>
            { setupDisplay() }
        </React.Fragment>
    )
}

export default Sidebar