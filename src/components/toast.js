const Toast = (toast) => {

    return {
        success: (title="", description="") => toast({
            title: title,
            description: description,
            status: "success",
            duration: 2000,
            isClosable: true,
        }),
        error: (title="", description="") => toast({
            title: title,
            description: description,
            status: "error",
            duration: 2000,
            isClosable: true,
        })
    }
}

export default Toast